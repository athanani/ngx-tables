import {Directive, HostListener} from '@angular/core';
import {ActivatedTableService} from '../tables.activated-table.service';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService, ErrorService, LoadingService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {ClientDataQueryable, ResponseError} from '@themost/client';
import { TableConfiguration } from '../components/advanced-table/advanced-table.interfaces';

export const MAX_EXPORT_ITEMS = 500;

export declare interface ExportLimitConfiguration {
  tables?: {
    exportLimit?: number
  };
}

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '.universis-btn-export'
})
export class BtnExpDirective {
  constructor(private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext,
              private _config: ConfigurationService,
              private _translate: TranslateService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService) {
  }

  @HostListener('click', ['$event'])
  async clickEvent(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this._activatedTable.activeTable) {
      return this._activatedTable.activeTable.exportView();
      /*
      const lastQuery = this._activatedTable.activeTable.lastQuery;
      const tableConfig = this._activatedTable.activeTable.config;
      const serviceHeaders = this._context.getService().getHeaders();
      const queryModel = lastQuery.getModel();
      let query = this._context.model(queryModel).asQueryable();
      const queryParams = lastQuery.getParams();
      query = this.constructQuery(query, queryParams, tableConfig, serviceHeaders);
      // get export settings
      const exportSettings = this._config.settings as ExportLimitConfiguration;
      // get export limit
      const maxExportItems = exportSettings && exportSettings.tables && exportSettings.tables.exportLimit;
      if (maxExportItems && typeof maxExportItems === 'number' && maxExportItems > 0) {
        query.skip(0).take(maxExportItems);
      } else {
        query.skip(0).take(MAX_EXPORT_ITEMS);
      }
      // This constructs the URI based on the way @themost does
      const url = this.constructURL(query);
      this.downloadFile(url, query.getService().getHeaders(), query.getModel()); */
    }
  }

  /**
   * Sets the parameters to the query based on
   * the active filters of the activated table
   * @param query
   * @param params: The filters of the last query of the activated table
   * @return ClientDataQueryable
   */
  setParams(query: ClientDataQueryable, params): ClientDataQueryable {
    if (params && typeof params === 'object') {
      Object.keys(params).forEach((key) => {
        query.setParam(key, params[key]);
      });
    }
    return query;
  }

  /**
   * Sets the headers to the query based on
   * the default headers of the DataContext
   * and appending the accept header to get
   * an xlsx file from the API
   * @param query
   * @param headers: The headers to append to the query
   * @return ClientDataQueryable
   */
  setHeaders(query: ClientDataQueryable, headers: Headers): ClientDataQueryable {
    Object.keys(headers).forEach((key) => {
      if (headers.hasOwnProperty(key)) {
        query.getService().setHeader(key, headers[key]);
      }
    });
    // sets a header to accept xlsx files
    query.getService().setHeader('Accept', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    return query;
  }

  /**
   * Constructs the select parameter of the
   * ClientDataQuery by replacing the target name
   * of the field based on the localized translation
   * of that property on the advanced tabled
   * @param query
   * @param tableConfig: The configuration of the activated table
   * @return ClientDataQueryable
   */
  constructQueryParam(query: ClientDataQueryable, tableConfig: TableConfiguration): ClientDataQueryable {
    let selectQuery = query.getParams().$select;
    let selectQueryArray = selectQuery.split(',');
    selectQueryArray = selectQueryArray.map((select) => {
      const words = select.split(' ');
      if (words.length === 3) {
        if (tableConfig && tableConfig.columns) {
          for (const column of tableConfig.columns) {
            if (column.property === words[2] && column.title) {
              words[2] = `"${this._translate.instant(column.title)}"`;
            } else if (column.property === words[2]) {
              words[2] = `"${this._translate.instant(words[2])}"`;
            }
          }
        }
      }
      if (words.length === 1) {
        if (tableConfig && tableConfig.columns) {
          for (const column of tableConfig.columns) {
            if (column.name === words[0] && column.title) {
              words[0] = `${words[0]} as "${this._translate.instant(column.title)}"`;
              break;
            } else if (column.name === words[0] && column.property) {
              words[0] = `${words[0]} as "${this._translate.instant(column.property)}"`;
              break;
            } else if (words[0] === column.name && words[0] === 'id' && column.hidden) {
              break;
            // tslint:disable-next-line:triple-equals
            } else if (words[0] == column.name || words[0] === column.property) {
              words[0] = `${words[0]} as "${this._translate.instant(words[0])}"`;
            }
          }
        }
        return words[0];
      }
      return words.join(' ');
    });
    selectQuery = selectQueryArray.join(',');
    query.setParam('$select', selectQuery );
    return query;
  }

  /**
   * Constructs the fetch URL based the query parameters
   * @param query
   * @return String
   */
  constructURL(query): String {
    const baseURL = query.getService().getBase();
    let url = baseURL.endsWith('/') ? (baseURL + query.getModel() + '?') : (baseURL + '/' + query.getModel() + '?');
    Object.keys(query.getParams()).forEach((key, index) => {
      if (index === 0) {
        url += key + '=' + query.getParams()[key];
      } else {
        url += '&' + key + '=' + query.getParams()[key];
      }
    });
    return url;
  }

  /**
   * Constructs the query by setting the parameters of the
   * advanced table's active filters, sets the necessary headers
   * and build the select parameter of the query
   * @param query
   * @param params: The filters of the last query of the activated table
   * @param tableConfig: The configuration of the activated table
   * @param headers: The headers to append to the query
   * @return ClientDataQueryable
   */
  constructQuery(query, params, tableConfig, headers: Headers) {
    let _query = this.setParams(query, params);
    _query = this.setHeaders(_query, headers);
    _query = this.constructQueryParam(_query, tableConfig);
    return _query;
  }

  /**
   * Fetches and saves the xlsx file
   * @param url
   * @param serviceHeaders: The headers to append to the query
   * @param modelName: The model name of the advanced table
   */
  downloadFile(url, serviceHeaders: Headers, modelName) {
    this._loadingService.showLoading();
    fetch(encodeURI(url), {
      headers: serviceHeaders,
      credentials: 'include'
    }).then(response => {
      if (response && !response.ok) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    }).then(blob => {
      const objectUrl = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = objectUrl;
      const downloadName = `${modelName}.xlsx`;
      a.download = downloadName;
      // this adds support for IE and MS Edge (up to version 44.19041)
      if (window.navigator && (window.navigator as any).msSaveOrOpenBlob) {
        (window.navigator as any).msSaveOrOpenBlob(blob, downloadName);
      } else {
        // for all other browsers
        a.click();
      }
      window.URL.revokeObjectURL(objectUrl);
      a.remove();
      this._loadingService.hideLoading();
    }).catch(err => {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

}
