import { Directive, OnDestroy, OnInit, Optional } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { AdvancedTableSearchComponent } from '../components/advanced-table/advanced-table-search.component';
import { NgxTablesState } from '../reducers';
import { AdvancedSearchState } from './advance-search-state.directive';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[routerSearchState]'
})
// tslint:disable-next-line:directive-class-suffix
export class AdvancedSearchRouterState extends AdvancedSearchState implements OnDestroy, OnInit {
  private _routerSubscription: Subscription;
  private _enteringState = true;
  private _applyState = false;

  constructor(
    protected search: AdvancedTableSearchComponent,
    @Optional() protected store: Store<NgxTablesState>,
    private readonly _router: Router
  ) {
    // call super constructor
    super(search, store);
    // set search state dynamically according to route
    this._routerSubscription = this._router.events
      .pipe(filter((event) => event instanceof NavigationStart || event instanceof NavigationEnd))
      .subscribe((event: NavigationStart | NavigationEnd) => {
        // capture urls regarding tab changing while the component is active
        if (event instanceof NavigationStart && !this._enteringState) {
          try {
            // set search state
            this.searchState = new URL(event.url, location.origin).pathname;
          } catch (err) {
            console.error('An error occured while trying to set search state for the current table', err);
            // fallback to event url
            this.searchState = event.url;
          }
          // toogle applyState
          this._applyState = true;
        }
        // capture url of the first activation of the component (e.g. coming from another route)
        if (event instanceof NavigationEnd && this._enteringState) {
          try {
            // set search state
            this.searchState = new URL(event.urlAfterRedirects, location.origin).pathname;
          } catch (err) {
            console.error('An error occured while trying to set search state for the current table', err);
            // fallback to event urlAfterRedirects
            this.searchState = event.urlAfterRedirects;
          }
          // toggle enteringState
          this._enteringState = false;
        }
        if (this._applyState) {
          // apply new table state
          super.applyTableState();
          this._applyState = false;
        }
      });
  }

  ngOnDestroy(): void {
    // destroy own subscriptions
    if (this._routerSubscription) {
      this._routerSubscription.unsubscribe();
    }
    // destroy parent subscriptions
    return super.ngOnDestroy();
  }
}
