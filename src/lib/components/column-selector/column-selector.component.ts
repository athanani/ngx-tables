// tslint:disable-next-line:max-line-length
import { Component, OnInit, OnDestroy, Input, EventEmitter, ViewEncapsulation, OnChanges, SimpleChanges, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterModalOkCancel } from '@universis/common/routing';
import { BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-column-selector',
  templateUrl: './column-selector.component.html',
  encapsulation: ViewEncapsulation.Emulated,
  styles: [
    `
    .form-select {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        padding: 5px;
    }
    .form-select * {
        padding: 5px 5px;
    }
    .btn.disabled, .btn:disabled {
        border-color: transparent !important;
    }
    `
  ]
})
export class ColumnSelectorComponent extends RouterModalOkCancel implements OnInit, OnDestroy, OnChanges {

    @Input() columns?: { key: string, title: string, visible: boolean, index: number }[];
    public dismiss: EventEmitter<any> = new EventEmitter();

    @Input() selectedColumns?: { key: string, title: string }[];

    @ViewChild('moveUpButton') moveUpButton?: ElementRef;
    @ViewChild('moveDownButton') moveDownButton?: ElementRef;

    availableColumnsControl = new FormControl();
    selectedColumnsControl = new FormControl();

    public changing = false;

    ok(): Promise<any> {
        this._changeDetector.detectChanges();
        setTimeout(() => {
            this.changing = true;
            this._modalRef.hide();
        }, 0);
        try {
            this.applyChanges();
        } catch (err) {
            throw err;
        } finally {
            this.changing = false;
        }
        this.dismiss.emit('ok');
        return Promise.resolve('ok');
    }
    cancel(): Promise<any> {
        this._modalRef.hide();
        this.dismiss.emit('cancel');
        return Promise.resolve('cancel');
    }

  constructor(router: Router,
    activatedRoute: ActivatedRoute,
    private _modalRef: BsModalRef,
    private _changeDetector: ChangeDetectorRef) {
        super(router, activatedRoute);
    }
    ngOnChanges(changes: SimpleChanges): void {
        //
    }
    ngOnDestroy(): void {
        //
    }
    ngOnInit(): void {
        if (Array.isArray(this.columns)) {
            this.selectedColumns = this.columns.filter((item) => {
                return item.visible;
            }).sort((a, b) => {
                if (a.index < b.index) {
                    return -1;
                }
                if (a.index > b.index) {
                    return 1;
                }
                return 0;
            }).map((item) => {
                return {
                    key: item.key,
                    title: item.title
                };
            });
        }
    }

    private applyChanges() {
        if (this.selectedColumns && this.columns) {
            let moveIndex = this.selectedColumns.length - 1;
            for (const column of this.columns) {
                const findIndex = this.selectedColumns.findIndex((item) => {
                    return item.key === column.key;
                });
                if (findIndex < 0) {
                    column.visible = false;
                    moveIndex += 1;
                    column.index = moveIndex;
                } else {
                    column.visible = true;
                    column.index = findIndex;
                }
            }
        }
    }

    removeColumns() {
        if (this.selectedColumns) {
            const selected = this.selectedColumnsControl.value as string[];
            if (selected.length > 0) {
                for (const value of selected) {
                    const findIndex = this.selectedColumns.findIndex((item) => {
                        return item.key === value;
                    });
                    if (findIndex >= 0) {
                        this.selectedColumns.splice(findIndex, 1);
                    }
                }
                // reset column
                this.selectedColumnsControl.reset();
            }
        }
    }

    addColumns() {
        if (this.columns && this.selectedColumns) {
            const selected = this.availableColumnsControl.value as string[];
            if (selected.length > 0) {
                for (const value of selected) {
                    const findColumn = this.columns.find((item) => {
                        return item.key === value;
                    });
                    if (findColumn) {
                        const findIndex = this.selectedColumns.findIndex((item) => {
                            return item.key === value;
                        });
                        if (findIndex < 0) {
                            this.selectedColumns.push({
                                key: findColumn.key,
                                title: findColumn.title
                            });
                        }
                    }
                }
                // find the last index of the selected items
                const lastSelectedIndex = this.columns.findIndex((item) => {
                    return item.key === selected[selected.length - 1];
                });
                // reset control
                this.availableColumnsControl.reset();
                if (lastSelectedIndex > -1 && lastSelectedIndex < this.columns.length - 1) {
                    this.availableColumnsControl.patchValue([
                        this.columns[lastSelectedIndex + 1].key
                    ]);
                }
            }
        }
    }

    moveUp() {
        if (this.moveUpButton) {
            this.moveUpButton.nativeElement.disabled = true;
        }
        if (this.selectedColumns) {
            const selected = this.selectedColumnsControl.value as string[];
            if (selected.length > 0) {
                for (const value of selected) {
                    const findIndex = this.selectedColumns.findIndex((item) => {
                        return item.key === value;
                    });
                    if (findIndex > 0) {
                        const column = this.selectedColumns[findIndex];
                        this.selectedColumns.splice(findIndex, 1);
                        this.selectedColumns.splice(findIndex - 1 , 0, column);
                    }
                }
            }
        }
        if (this.moveUpButton) {
            this.moveUpButton.nativeElement.disabled = false;
        }
    }

    moveDown() {
        if (this.moveDownButton) {
            this.moveDownButton.nativeElement.disabled = true;
        }
        if (this.selectedColumns && this.columns) {
            const selected = this.selectedColumnsControl.value as string[];
            if (selected.length > 0) {
                for (const value of selected) {
                    const findIndex = this.selectedColumns.findIndex((item) => {
                        return item.key === value;
                    });
                    const length = this.columns.length;
                    if (findIndex >= 0 && findIndex < length) {
                        const column = this.selectedColumns[findIndex];
                        this.selectedColumns.splice(findIndex, 1);
                        this.selectedColumns.splice(findIndex + 1 , 0, column);
                    }
                }
            }
        }
        if (this.moveDownButton) {
            this.moveDownButton.nativeElement.disabled = false;
        }
    }
}
