import {Component, Input, OnInit, Output, ViewChild,
  ViewEncapsulation, EventEmitter, AfterViewInit, TemplateRef, OnDestroy} from '@angular/core';
import { AdvancedTableComponent } from './advanced-table.component';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { UserStorageService, ModalService, DIALOG_BUTTONS } from '@universis/common';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedFilterValueProvider} from './advanced-filter-value-provider.service';
import { AdvancedTableSearchBaseComponent } from './advanced-table-search-base';
import {TemplatePipe} from '@universis/common';
import { Subscription } from 'rxjs';
import { TextUtils } from '@themost/client';

export interface AdvancedSearchViewStatus {
  advancedView: boolean;
}

export interface AdvancedSearchList {
  name: string;
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-advanced-table-search',
  templateUrl: './advanced-table-search.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class AdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent implements OnInit, AfterViewInit, OnDestroy  {

  private queryParamSubscription?: Subscription;
  private configChangesSubscription?: Subscription;
  @Input() public table?: AdvancedTableComponent;
  @Input() public collapsed = true;
  @Input() public allowSave = true;
  @Input() public showMore = true;
  @Input() public mergeQueryParams = false;
  @ViewChild('searchText') public searchText: any;
  @Output('viewChange') public viewChange: EventEmitter<AdvancedSearchViewStatus> = new EventEmitter<AdvancedSearchViewStatus>();
  @Output('listChange') public listChange: EventEmitter<AdvancedSearchList> = new EventEmitter<AdvancedSearchList>();
  public set text(value: string) {
    if (this.searchText) {
      this.searchText.nativeElement.value = value;
    }
  }

  public get text(): string {
    return (this.searchText && this.searchText.nativeElement && this.searchText.nativeElement.value) as string;
  }

  public modalRef?: BsModalRef;
  public storageKey: any;
  public storageValue: any;
  public userSearchList: any = [];
  public selectedList: any = null;

  constructor( protected _context: AngularDataContext,
               protected _activatedRoute: ActivatedRoute,
               protected datePipe: DatePipe,
               private _modalService: BsModalService,
               private _userStorage: UserStorageService,
               private _translate: TranslateService,
               private _advancedSearchValueProvider: AdvancedFilterValueProvider,
               private _template: TemplatePipe,
               private _router: Router,
               private _modal: ModalService) { super(); }

  public ngOnInit(): void {
    if (this.table == null) {
      return;
    }
    this.configChangesSubscription = this.table.configChanges.subscribe((config) => {
        if (config == null) {
          return;
        }
        // load user searches
        this.loadUserSearchList().then(() => {
          // if search component merges query params
          // (uses queryParams to apply filter)
          if (this.mergeQueryParams) {
            this.queryParamSubscription = this._activatedRoute.queryParams.subscribe((queryParams) => {
              this.applyQueryParams(queryParams);
            });
          }
        });
      });
  }

  /**
   * Applies query given by the queryParams of the activated route
   * @param queryParams
   */
  private applyQueryParams(queryParams: any) {
    if (this.table == null) {
      return;
    }
    const table = this.table;
    this.filter = {};
    if (Object.prototype.hasOwnProperty.call(queryParams, 'q')) {
      // set collapsed
      this.collapsed = true;
      // search text
      this.text = queryParams.q;
      return table.search(this.text);
    } else {
      this.text = '';
    }

    let filterParams = Object.assign({}, queryParams);

    if (Object.prototype.hasOwnProperty.call(queryParams, 's')) {
      // set collapsed
      this.collapsed = false;
      // find user search by name
      if (this.userSearchList) {
        this.selectedList = this.userSearchList.find((item) => {
          return item.name === queryParams.s;
        });
        if (this.selectedList) {
          filterParams = this.selectedList.filter;
        }
      }
    } else {
      this.selectedList = null;
    }

    Object.keys(filterParams).filter((key) => {
      // search criteria
      if (table.config && table.config.criteria) {
        return table.config.criteria.find((item) => {
          return item.name === key;
        });
      }
    }).forEach((key) => {
      let value = filterParams[key];
      if (TextUtils.isNumber(value)) {
        value = parseFloat(filterParams[key]);
      }
      if (this.filter[key] !== value) {
        // set value
        Object.defineProperty(this.filter, key, {
          configurable: true,
          enumerable: true,
          writable: true,
          value,
        });
      }
      if (this.filter[key] != null) {
        this.collapsed = false;
      }
    });

    // call filterChange event to apply change to advanced search children
    this.filterChange.emit(this.filter);
    // apply query
    this.getQuery().then((query) => {
      // if table query is null
      if (table.query == null) {
        // set query
        table.query = query;
      } else {
        if (query != null && query.getParams()['$filter']) {
          const params = query.getParams();
          // otherwise set filter params
          table.query.setParam('$filter', params.$filter);
        } else {
          table.query.setParam('$filter', null);
        }
      }
      // and finally fetch data
      table.fetch(true);
    });
  }

  public ngOnDestroy(): void {
    if (this.queryParamSubscription) {
      this.queryParamSubscription.unsubscribe();
    }
    if (this.configChangesSubscription) {
      this.configChangesSubscription.unsubscribe();
    }
  }

  public ngAfterViewInit(): void {

  }

  public onSearchKeyDown(event: any) {
    if (this.table && event.keyCode === 13) {
      // get search text
      const searchText = ( event.target as HTMLInputElement).value;
      // merge query params ?
      if (this.mergeQueryParams) {
        const queryParams = {
          q: searchText,
        };
        if (this.table.config && this.table.config.criteria) {
          this.table.config.criteria.forEach((item) => {
            Object.defineProperty(queryParams, item.name, {
                configurable: true,
                enumerable: true,
                writable: true,
                value: null,
              });
          });
        }
        // try to apply filter by appending query params
        return this._router.navigate([], {
          relativeTo: this._activatedRoute,
          queryParams,
          queryParamsHandling: 'merge',
        });
      }
      this.filterChange.emit({
        q: searchText
      });
      // otherwise do search
      this.table.search(searchText);
      return false;
    }
  }

  public transformDate(date) {
    date = this.datePipe.transform(date, 'MM/dd/yyyy'); // whatever format you need.
    return date;
  }

  public convertToString(nameFilter) {
    let name = '';
    if (nameFilter != null) {
      name = nameFilter.toString();
    }
    return name;
  }

//  Load saved criteria searches user has stored before
  private loadUserSearchList(): Promise<void> {
    if (this.table == null) {
      return Promise.resolve();
    }
    this.storageKey = 'tables/' + this.table.config.model + '/searches';
    return this._userStorage.getItem(this.storageKey).then((data) => {
      const list = data.value || [];
      // if table configuration has pre-defined searches
      if (this.table && this.table.config.searches && this.table.config.searches.length) {
        // add pre-defined lists
        list.push.apply(list, this.table.config.searches.map ( (x) => {
          x.readOnly = true; // as readonly
          x.name = this._translate.instant(x.name);
          return x;
        }));
      }
      // set user search list
      this.userSearchList = list;
      return Promise.resolve();
    }).catch((err) => {
      // DO NOT THROW ERROR
      // todo: notify component for this error in order to disable some actions
      console.error('An error occurred while getting user search list');
      console.error(err);
      return Promise.resolve();
    });
  }

  public onSelectUserSearch(event: any): any {
    if (this.selectedList && this.table) {
      const listFound = this.searchInListByName(this.selectedList.name);
      if (listFound) {
        if (this.mergeQueryParams) {
          const queryParams = { q: null, s: this.selectedList.name };
          // clear criteria if any
          this.table.config.criteria.forEach((item) => {
            if (Object.prototype.hasOwnProperty.call(queryParams, item.name) === false) {
              // set null value
              Object.defineProperty(queryParams, item.name, {
                configurable: true,
                enumerable: true,
                writable: true,
                value: null,
              });
            }
          });
          return this._router.navigate([], {
            relativeTo: this._activatedRoute,
            queryParams,
            queryParamsHandling: 'merge',
          });
        }
        this.selectedList = listFound;
        this.filter = this.selectedList.filter;
        this.filterChange.emit(this.filter);
        return true;
      }
    } else {
      return this._router.navigate([], {
        relativeTo: this._activatedRoute,
        queryParams: { s: null },
        queryParamsHandling: 'merge',
      });
    }
    return false;
  }

  public searchInListByName(name: string): any {
    if ( Array.isArray(this.userSearchList) ) {
      const listFound = this.userSearchList.find( (x) => x.name === name);
      if ( listFound ) {
        return listFound;
      } else {
        return false;
      }
    }
  }

//  Update list item contents with given content (Search by object name property)
  public updateSavedSearchListItemByName( name: string, newItem: any ) {
    const itemInList = this.userSearchList.find( (x) => x.name === name );
    const index = this.userSearchList.indexOf(itemInList);
    this.userSearchList[index] = newItem;
    this.selectedList = this.userSearchList[index];
  }

//  Update list item contents with given content (Search by object)
  public updateSavedSearchListItemByObject( itemInList: any, newItem: any ) {
    const index = this.userSearchList.indexOf(itemInList);
    this.userSearchList[index] = newItem;
    this.selectedList = this.userSearchList[index];
  }

//  Delete list item from list
  public deleteItemFromSavedSearchListByObject( itemInList: any) {
    this.userSearchList = this.userSearchList.filter((item) => item !== itemInList);
    this.selectedList = null;
    this.filter = {};
    this.advancedSearch(true);
    this.uploadListOnUserStorage();
  }

//  User chose to save its advanced form search criteria
  public saveSearch( templateToLoad: TemplateRef<any>, newList: boolean = true) {
    if (this.table && this.table.config.model) {
//      Set the corresponding key to its list and show a confirmation modal window
      this.storageKey = `tables/${this.table.config.model}/searches`;
      if (this.filter && Object.keys(this.filter).length) {
//      User wants to save an existing list from the dropdown
        if ( !newList && this.selectedList ) {
          const newItem = { name :  this.selectedList.name, filter : { ... this.filter } };
          this.updateSavedSearchListItemByObject ( this.selectedList, newItem );
          this.uploadListOnUserStorage();
//      User wants to save a new list
        } else {
          this.modalRef = this._modalService.show(templateToLoad);
        }
      } else {
//        User did not define any criteria
        this._modal.showWarningDialog(
          this._translate.instant('AdvancedSearch.Modal.Title'),
          this._translate.instant('AdvancedSearch.Modal.EmptyCriteriaList'),
          DIALOG_BUTTONS.Ok);
      }
    } else {
      this._modal.showWarningDialog(
          this._translate.instant('AdvancedSearch.Modal.Title'),
          this._translate.instant('AdvancedSearch.Modal.NoListDefined'),
          DIALOG_BUTTONS.Ok);
    }
  }

//  User Accepted => Save list and Close modal
  public confirmModal( saveFormName ): void {
//    name given and list name is unique
    if ( saveFormName && !(this.searchInListByName(saveFormName)) ) {
      const newInsertedValue = { name :  saveFormName, filter : { ... this.filter } };
      if ( this.userSearchList && this.userSearchList.length !== 0 ) {
        this.userSearchList.push(newInsertedValue);
        this.storageValue = this.userSearchList;
        this.selectedList = newInsertedValue;
      } else {
        this.storageValue = [newInsertedValue];
        this.userSearchList = this.storageValue;
        this.selectedList = newInsertedValue;
      }
      this.uploadListOnUserStorage();
      if (this.modalRef) {
        this.modalRef.hide();
      }
    } else if ( saveFormName && (this.searchInListByName(saveFormName)) ) {
//      User should insert a different name cause this one is already in use
      alert(this._translate.instant('AdvancedSearch.Modal.TitleOnListAlreadyCreated'));
    } else {
//      User did not insert any text
      alert(this._translate.instant('AdvancedSearch.Modal.TitleOnUserError'));
    }
  }

//  User declined => Close modal
  public closeModal(): void {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

//  Upload user's criteria on User Storage
  public uploadListOnUserStorage() {
    this._userStorage.setItem(this.storageKey, this.userSearchList.filter((x) => {
      return !x.readOnly;
    }));
  }

  /**
   * @deprecated This method is going to be deprecated. Use search() method instead.
   * @param event
   */
  public async asyncAdvancedSearch(event?: any) {
    if ( this.table && this.table.config && Array.isArray(this.table.config.criteria)) {
      const query = await this.getQuery();
      if (this.mergeQueryParams) {
        this.selectedList = null;
        const queryParams = Object.assign({ q: null, s: null }, this.filter);
        // enumerate criteria
        this.table.config.criteria.forEach((item) => {
          if (Object.prototype.hasOwnProperty.call(queryParams, item.name) === false) {
            // set null value
            Object.defineProperty(queryParams, item.name, {
              configurable: true,
              enumerable: true,
              writable: true,
              value: null,
            });
          }
        });
        return this._router.navigate([], {
          relativeTo: this._activatedRoute,
          queryParams,
          queryParamsHandling: 'merge',
        });
      }

      if (this.table.query && query && query.getParams()['$filter']) {
        this.table.query.setParam('$filter', query.getParams()['$filter']);
      } else {
        this.table.query = query;
      }
      this.filterChange.emit(this.filter);
      this.table.fetch();
    }
  }

  public search(): Promise<void> {
    return this.asyncAdvancedSearch().then(() => {
      // do nothing
    });
  }

  public async getQuery() {
    // on simple search return null
    if (this.collapsed) {
      return null;
    }
    if ( this.table && this.table.config && Array.isArray(this.table.config.criteria)) {
      const expressions: any[] = [];
      const values = await this._advancedSearchValueProvider.getValues();
      this.table.config.criteria.forEach((x) => {
        if (Object.prototype.hasOwnProperty.call(this.filter, x.name)) {
          const nameFilter = this.convertToString(this.filter[x.name]);
          if (nameFilter.includes('GMT')) {
            const newDate = this.transformDate(this.filter[x.name]);
            expressions.push(this._template.transform(x.filter, {
              value: newDate,
            }));
          } else if (this.filter[x.name] !== 'undefined') {
            expressions.push(this._template.transform(x.filter, Object.assign({
              value: this.filter[x.name],
            }, values)));
          }
        }
      });
      let query;
      if (expressions && expressions.length) {
        // create client query
        query = this._context.model(this.table.config.model).asQueryable();
        query.setParam('filter', expressions.join(' and '));
      } else {
        query = this._context.model(this.table.config.model).asQueryable();
      }
      return query;
    }
  }

  public advancedSearch(event: any) {
    return this.asyncAdvancedSearch(event);
  }

  public clear() {
    if (this.mergeQueryParams && this.table) {
        this.selectedList = null;
        const queryParams = { q: null, s: null };
        // enumerate criteria
        this.table.config.criteria.forEach((item) => {
          if (Object.prototype.hasOwnProperty.call(queryParams, item.name) === false) {
            // set null value
            Object.defineProperty(queryParams, item.name, {
              configurable: true,
              enumerable: true,
              writable: true,
              value: null,
            });
          }
        });
        return this._router.navigate([], {
          relativeTo: this._activatedRoute,
          queryParams,
          queryParamsHandling: 'merge',
        });
      } else {
      // clear filter
      this.filter = {};
      // clear list
      this.selectedList = null;
      // raise filter change event
      this.filterChange.emit(this.filter);
    }
  }

  /**
   * Gets the view state of search control
   */
  public get viewState(): AdvancedSearchViewStatus {
    return {
      advancedView: !this.collapsed,
    };
  }

  /**
   * Switches view between simple and advanced search
   */
  public async toggleView() {
    this.collapsed = !this.collapsed;
    // emit event for view status change
    this.viewChange.emit(this.viewState);
  }
}
